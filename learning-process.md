
# Learning-process

### Question 1. What is the Feynman Technique? Explain in 1 line.

 1. Feynman Technique explain about how to learn concept quickly and easily. Explain about how to test your knowledge on that concepts.

### Question 2. In this video, what was the most interesting story or idea for you?
 
 1. Barbara Oakley story is most interesting.The idea of using two modes for quick learning and solving the problems.

### Question 3. What are active and diffused modes of thinking?

 1. In the active mode you are focus ,concentrated , actively engaged while doing the work .
 2. In the diffused mode your mind relaxed and you may not concentrated while doing the work .


### Question 4. According to the video, what are the steps to take when approaching a new topic? Only mention the points.

 1. Deconstruct the skill
 2. Learn enough to self-correct.
 3. Remove barriers to practice.
 4. Practice for at least 20 hours.
 
### Question 5. What are some of the actions you can take going forward to improve your learning process?

 1. To improve my learning process , I follow the Feynman Technique to learn the concept quickly 
 2. Test the knowledge of concept by writing in the note with simple word. 




