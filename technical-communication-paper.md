# Caching

## 1. Introduction:

 - In computer science and information technology, caching is a method for storing and retrieving data more effectively. 
 - To speed up the processing of incoming requests for data, it involves temporarily storing the data in a location known as a cache. 
 -  Caching is commonly used to improve system efficiency, minimize latency, and maximize resource management.
 
## 2. Purpose of Caching:

 - Caching is typically employed to speed up data retrieval and minimize the load on slower storage systems or remote resources.   
 - Applications can respond to queries faster by caching frequently accessed data.
 
 ## 3. Cache Types:

 - Depending on the location and method of data storage, there are several kinds of caching. Typical cache kinds consist of:
    - In-Memory Caching: Storing data in the computer's RAM for rapid access.
    - Database Caching: Storing query results or frequently used database records.
    - Web Caching: Storing web content like images, scripts, and pages closer to end-users using content delivery networks (CDNs).
    - Browser Caching: Storing web assets like styles and scripts in the user's browser cache.
    
 ## 4. Cache Keys and Values:

 - A key-value data structure is employed by caches. 
 - Providing a key that matches the stored value allows for the retrieval of data.

 ## 5. Cache Hit and Cache Miss Analysis:
 - A "cache miss" occurs when data that is requested from a cache is not found, whereas a "cache hit" occurs when the requested data is located in the cache. 
 - Cache misses need retrieving data from the original source, but cache hits are quicker because the data is instantly accessible.

 ## 6. Cache Replacement Policies:
 
 - Algorithms in caches are implemented for handling limited store space. 
 - First-In-First-Out (FIFO), Most Recently Used (MRU), and Least Recently Used (LRU) are common cache replacement rules.
 
 ## 7. Applications and Scenarios:

 - Caching is widely used in many different applications, including distributed systems, databases, content delivery networks (CDNs), web browsers (browser caching), and operating systems. 
 - It is beneficial for enhancing the functionality of websites and applications.

 ## 8. Conclusion:
 
 - In computer science and software engineering, caching is a basic and adaptable method. 
 - Increasing system performance, reducing latency, and optimising resource use are its main goals.
 
 ## 9. References
      
 - https://www.youtube.com/watch?v=6GY1akbxyEo
 - https://www.youtube.com/watch?v=IaDU8_KjrpY
 - https://www.youtube.com/watch?v=IaDU8_KjrpY
 
