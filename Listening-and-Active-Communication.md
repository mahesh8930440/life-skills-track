# Listening-and-Active-Communication

## Question 1. What are the steps/strategies to do Active Listening? (Minimum 6 points)
 
 1. The listener is advised to focus on the speaker and topic
 2. Avoiding interruption until they have finished speaking. 

### Question 2.According to Fisher's model, what are the key points of Reflective Listening? (Write in your own words, use simple English)

 1. It is important to pay close attention to the speaker 
 2. Telling the speaker again about his suggestion to make sure it was understood correctly.

### Question 3. What are the obstacles in your listening process?

 1. Personal reflections, outside distractions,social Media and video games

### Question 4. What can you do to improve your listening?  

 1. Improving your listening skills is essential for effective communication and building strong relationships

### Question 5. When do you switch to Passive communication style in your day to day life?

 1. When we want to avoid a Conflict
 2. When we're attempting to keep a good relationship with someone or avoid taking on responsibility

### Question 6. When do you switch into Aggressive communication styles in your day to day life 
 
 1. When I feel like my rights are being violated, when I'm defending myself

### Question 7. When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
 
 1. when I feeling disrespected 
 2. Avoiding discussions on sensitive family matters.
 3. when I feel jealousy or insecure

### Question 8. How can you make your communication assertive?  
 
  1. Expressing your thoughts, feelings, and needs in a clear, honest, and respectful manner .
  2. I'll stay within my comfort zone.
  