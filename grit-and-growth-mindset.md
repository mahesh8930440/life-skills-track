## Question 1. Paraphrase (summarize) the video in a few (1 or 2) lines. Use your own words.
  
   - Grit defines as the ability to learn and achieve success despite setbacks. Grit, or passion and perseverance, is     important for success  in life.

## Question 2. Paraphrase (summarize) the video in a few (1 or 2) lines in your own words.
   
   - Skills and intelligence may be developed, and we must improve our learning methods. We need to have faith in     themselves and    our ability to learn and improve. Instead of running away from difficulties, we must stand tall and face them. 

##  Question 3. What is the Internal Locus of Control? What is the key point in the video? 
   
   - Locus of control defines a person's ability to motivate themselves and control the events and outcomes in their lives. Spend less time on easy tasks and more time on difficult tasks.
   
## Question 4. What are the key points mentioned by speaker to build growth mindset (explanation not needed).
   
   - Believing in your ability to figure things out.
   - Learning from failures and setbacks.
   - Using failures as opportunities for growth.
   - Being open to learning and questioning yourself.

## Question 5. What are your ideas to take action and build Growth Mindset?
   
   - Make learning a lifelong commitment. Stay up-to-date with industry and always look for ways to expand your knowledge.
   - Learning from Feedback

 

