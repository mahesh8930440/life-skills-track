## 1. In this video, what was the most interesting story or idea for you? 
 - Idea is all about starting with something small and making it a habit. 
 - Starting small when you want to make new habits. Instead of trying to do something big right away.

## 2. How can you use B = MAP to make making new habits easier? What are M, A and P.
 -  B = MAP, where B stands for Behavior, M for Motivation, A for Ability, and P for Prompt. To develop a new habit or make behavior change easier.
 -  To make new habits easier, you can boost your motivation by finding a way to connect the behavior to something you      already enjoy or value.
 -  To make new habits easier start small.

## 3. Why it is important to "Shine" or Celebrate after each successful completion of habit? (This is the most important concept in today's topic. Whatever you celebrate becomes a habit)
 - Celebrating or "Shining" after each successful completion of a habit is important for several reasons.
 -  Celebrating a successful habit completion creates a positive mindset with the behavior. Your brain registers the completion of the habit as a positive experience, which makes you more likely to repeat it in the future.

## 4. In this video, what was the most interesting story or idea for you?
 - Idea in the video is the concept of "the aggregation of marginal gains." 
 - This means that when you make lots of tiny improvements, like testing massage gels or getting better sleep, even a 1% change in each, it can add up to a big success. It's like collecting small drops of water to fill a big bucket.
 - This idea shows that if you keep making small, steady improvements over time, you can achieve great things

## 5. What is the book's perspective about Identity?
 - The book's perspective on identity is that it plays a significant role in habit formation and behavior change.
 - The book says that if you want to change your habits, it's really powerful to change how you see yourself. Instead of only thinking about outside goals, you should make your habits match the kind of person you want to be.

## 6. Write about the book's perspective on how to make a habit easier to do?
 - Begin with a very simple version of the habit, like a two-minute task, so it feels easier to begin and that motivate you.
 
## 7.Write about the book's perspective on how to make a habit harder to do?
 - In "Atomic Habits," James Clear teaches how to not only make good habits easier but also make bad habits harder. 
 - He shares practical methods like creating obstacles for bad habits, hiding tempting items, using commitment tools, adding time delays, and making bad habit consequences more visible.
 - These ideas help people resist bad habits and form better ones, offering a comprehensive guide to habit change.
## 8. Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
 - I want to meditate every day more as a habit. Numerous advantages of meditation exists for both mental and physical health, such as heightened mindfulness, improved focus, and decreased stress.
 - Set a specific time and place for meditation. For example, I could meditate for 10 minutes every morning right after I wake up
 - Find a style of meditation that I enjoy.

## 9.Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
 - I want to stop spending too much time on social media without any purpose because it's not a good use of my time and doesn't help me get things done.
 - Taking the social media apps off the main screen of my phone to make them harder to see and use.
 



 

