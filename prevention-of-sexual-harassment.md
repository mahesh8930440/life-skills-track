
# Prevention-of-sexual-harassment

### Questions 1. What kinds of behaviour cause sexual harassment?
 
 1. Sexual harassment is any unwelcome verbal, visual, or physical conduct of a sexual nature that affects working conditions .
 2. Treating someone unfairly on the basis of gender.
 3. Physical touch that causes discomfort in others.
 4. Sexual or gender-based jokes, comments on a person's appearance, or requests for sexual favours are all prohibited.
 5. Without permission, touching or grabbing. 

### Questions 2. What would you do in case you face or witness any incident or repeated incidents of such behaviour?
  
 1. Providing support to the victim is crucial. 
 2. Encourage them to speak out and ensure they know they are not alone.
 3. Reporting the incident to a supervisor, human resources.
 4. Reporting the issue to the police.
